# Y&R HTML/CSS test

The HTML/CSS test is about implementing a web page according the provided
template 'loremsite_index_1024.psd'. We also implemented a simple end-to-end
test.

## Design and responsiveness

Based on the template (loremsite_index_1024.psd), even if it is built on a
1170px grid which fit with Bootstrap, we decided to not use such CSS framework
and go for implement our own fluid grid with some CSS classes.
The responsive design covers screen sizes from 320px width to 1920px width.
```css
.col-xs , .col-s, .col-m, .col-ml, .col-l {
	margin-left: 1%;
	margin-right: 1%;	
}

.col-xs {
	width: 23%;	
}

.col-s {
	width: 31.33%;
}

.col-m {
	width: 48%;
}

.col-ml {
	width: 64.67%;
}

.col-l {
	width: 98%;
}

.row {
	width: 100%;	
}
```

/////

As the font Segoe font from the template is not free, we used a similar free
font called 'Open Sans'. To embed the font into the CSS file, we use a Google
API :
```css
@import url('https://fonts.googleapis.com/css?family=Open+Sans');
```

/////

For the various icons, we used the [Font Awesome](http://fontawesome.io)
collection.

## Front-end development

For achieve the goal, we used the following languages: HTML5 + CSS3/Less +
JavaScript (ECMAScript 5) For styling, we used Less, a CSS pre-processor which
is compile on client side with a less.js file, as we are more focused on
responsive design than performance (client side compilation is more greedy).
```html
<script src="src/less.min.js" type="text/javascript"></script>
```

## Browser compatibility

To be sure about the compatibily of the web page with the latest versions of
various browsers (IE, Edge, Safari, Firefox, Chrome), and to be inline with W3C
standard we tested it at mutliple times with :
* [W3C Markup Validation Service](https://validator.w3.org) - checking the index.html file
* [BrowserStack](https://www.browserstack.com) - mainly used for testing on Edge 16
* [Browserling](https://www.browserling.com) - only used for testing on IE 11
and at once on Safari (as we are coding on a Mac OS environment), Firefox and
Chrome browsers

## Prerequisites

A local server, like a http server, can be used to launch the web page.
To download it, npm can be used with the following command :
```npm
npm install http-server -g
```
to run a server, use the following command at the root of the folder where the
html file is :
```npm
http-server
```

/////

For the e2e tests, there is no requirements as we already installed and set up
Nightwatch and Selenium (all in tests_e2e folder).

## installation

After dowloading the app folder here, simply fire the index.html. At the root of
the project, lauch a server with the command :
```npm
http-server.
```

Or simply drag & drop the index.html file into a Firefox window for exemple to
see the result.

## E2E testing

For the E2E test part, we are using [Nightwatch.js](http://nightwatchjs.org), an
automated testing framework written on [Node.js](https://nodejs.org/en/about),
coupled with a [Selenium web driver](https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol).

We are simply testing here if the web page appearing as expected, if the picture
gallery appear as expected when clicking on a dedicated button and if the buttons
for the mobile navigation are reacting.

We stored the result of the web page on internet [here](http://lignographic.free.fr/test.html)
and we use this URL like this on the tests.e2eJs file which contains various tests :
```
module.exports = {
  'header should be visible': function(browser){
    browser
      .url('http://lignographic.free.fr/test.html')
      .waitForElementVisible('header', 1000)
      .end();
  }
}
```

For launching the tests, at the root where is the 'tests_e2e' folder, use the
following command :
```
tests_e2e/lib/node_modules/.bin/nightwatch
```

## API reference

* [Nightwatch](http://nightwatchjs.org/api) - the automated testing framework used
* [Selenium web driver](https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol) - the Selenium tool used and coupled with Nightwatch
* [Node.js](https://nodejs.org/api) - a JavaScript runtime
* [npm](https://docs.npmjs.com) - a package manager
* [http-server](https://www.npmjs.com/package/http-server) - zero-configuration http server (for testing and local development)
