window.onload = function(){

/* ===== FUNCTIONS ===== */

/* ===== move on slide ===== */
function plusSlides(n){
	showSlides(slideIndex += 1);
}


/* ===== show slides ===== */
function showSlides(n){
	var i;
	var slides = document.getElementsByClassName('my-slides');
	if(n > slides.length){
		slideIndex = 1
	}
	if(n < 1){
		slideIndex = slides.length
	}
	for(i = 0; i < slides.length; i++){
		slides[i].style.display = 'none';
	}
	slides[slideIndex-1].style.display = 'block';
}


/* ===== display slider ===== */
function displaySlider(){
	var slider = document.querySelector('.slider-container');
	var overlay = document.querySelector('.overlay');
	overlay.classList.toggle('overlay-visible');
	slider.classList.toggle('slider-container-visible');
	console.log(overlay.classList.contains('overlay-visible'));
}

var slideIndex = 1;
showSlides(slideIndex);



/* ===== EVENT HANDLERS ===== */

var navBtn = document.getElementById('btn-nav-mobile');
var closeBtn = document.querySelector('.close-nav-mobile');
var nav = document.getElementById('nav-mobile-block');

var btnShowSlider = document.querySelector('#btn-o-yaku-gallery');
var close = document.querySelector('.cross');
var prev = document.querySelector('.prev');
var next = document.querySelector('.next');

var login = document.getElementById('login-account');


/* ===== sign in ===== */
login.addEventListener('click', function(){
	alert('a sign in popup should appear');
});

/* ===== mobile navigation ===== */
navBtn.addEventListener('click', function(){
	nav.classList.add('nav-overlay-visible');
	//console.log(nav.classList.contains('nav-overlay-visible'));
});

closeBtn.addEventListener('click', function(){
	nav.classList.remove('nav-overlay-visible');
	//console.log(nav.classList.contains('nav-overlay-visible'));
});


/* ===== actions on slider ===== */
btnShowSlider.addEventListener('click', function(){
	if(window.innerHeight > window.innerWidth){
	alert('please use the landscape mode to enjoy the galerie');
	} else {
		displaySlider();
	}
});

close.addEventListener('click', function(){
	displaySlider();
});

next.addEventListener('click', function(){
	plusSlides(1);
});

prev.addEventListener('click', function(){
	plusSlides(-1);
});



} // END window.onload function