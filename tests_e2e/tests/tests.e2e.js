module.exports = {

    'body should be visible': function(browser){
        browser
            .url('http://lignographic.free.fr/test.html')
            .waitForElementVisible('body', 5000)
            .end();
    },

    'the diaporama visible when clicking on the dedicated button': function(browser){
        browser
            .url('http://lignographic.free.fr/test.html')
            .waitForElementVisible('#btn-o-yaku-gallery', 1000)
            .click('.btn-o-yaku')
            .waitForElementVisible('.slider-container', 3000)
            .end();
    },

    'testing the mobile navigation buttons': function(browser){
        browser
            .url('http://lignographic.free.fr/test.html')
            .verify.elementPresent('#btn-nav-mobile')
            .click('#btn-nav-mobile')
            .assert.cssClassPresent('#nav-mobile-block', 'nav-overlay')
            .verify.elementPresent('.close-nav-mobile')
            .click('.close-nav-mobile')
            .end();
    }
}